# get_closest_reference_genome.py
Python tool which easily retrieves the closest good quality reference genome of a given organism
## Installation
`git clone https://github.com/smlatorreo/get_closest_reference_genome.git`
## Use
```bash
cd get_closest_reference_genome/
./get_closest_reference_genome.py
```
### To update the databse of reference genomes
`./get_closest_reference_genome.py update`
## License
GPL
## Author
Sergio Latorre
## Contact
smlatorreo@gmail.com
