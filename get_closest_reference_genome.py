#!/usr/bin/env python
__author__ = "Sergio Latorre"
__license__ = "GPL"
__email__ = "sergio.latorre@tuebingen.mpg.de"

from sys import exit
try:
    from Bio import Entrez
except ImportError:
    exit("Biopython module is missing. Try to install it by typing: pip install Biopython")
from urllib2 import urlopen
import pickle
from sys import argv

# Update references
try:
    if argv[1] == "update":
        from os import remove
        remove("references.pkl")
except IndexError:
    pass

# Enter valid e-mail adress
try:
    Entrez.email = (open("email_address.txt", "r")).readline()
except IOError:
    Entrez.email = raw_input("Please enter your e-mail address: ")
    print("\n")
    with open("email_address.txt", "w") as address:
        address.write(Entrez.email)
        address.close()

# Functions

def tax_id(name):
    try:
        first = Entrez.esearch(term = name, db = "taxonomy", retmode = "xlm")
        return (Entrez.read(first))["IdList"][0]
    except IndexError:
        exit("Wrong name!")

def data_tax(idtax):
    first = Entrez.efetch(id = idtax, db = "taxonomy", retmode = "xml")
    return Entrez.read(first)

# References
try:
    ref = pickle.load(open("references.pkl", "rb"))
except IOError:
    print("Constructing references database. It might take a while...")
    urls = {"plants" : urlopen("ftp://ftp.ensemblgenomes.org/pub/plants/current/species_EnsemblPlants.txt"),
    "fungi" : urlopen("ftp://ftp.ensemblgenomes.org/pub/fungi/current/species_EnsemblFungi.txt"),
    "protists" : urlopen("ftp://ftp.ensemblgenomes.org/pub/protists/current/species_EnsemblProtists.txt"),
    "metazoa" : urlopen("ftp://ftp.ensemblgenomes.org/pub/metazoa/current/species_EnsemblMetazoa.txt")}
    references = []
    for group in urls:
        for line in urls[group]:
            if line.startswith("#"):
                continue
            else:
                references.append(line.split("\t")[3])
    ref = {}
    for reference in references:
        data_ref = data_tax(int(reference))
        ref[data_ref[0]["ScientificName"]] = data_ref[0]["LineageEx"]
    with open("references.pkl", "wb") as f:
        pickle.dump(ref, f, pickle.HIGHEST_PROTOCOL)
        f.close()

# Interface
name = raw_input("//////////////////////////////////////\n// Please enter Genus or Species    //\n// Examples:                        //\n// Genus: Phaseolus                 //\n// Species: Pennisetum clandestinum //\n//////////////////////////////////////\n\nEnter genus / species: ")
print("\n")

# Query species
idtax = tax_id(name)
data = (data_tax(idtax))[0]["LineageEx"]
dat = []
for i in range(len(data)-1, -1, -1):
    dat.append(data[i]["ScientificName"])

# Search and results

for species in ref:
    if name in species.split(" "):
        print("Your best option is: %s" % species)
for species in ref:
    if name in species.split(" "):
        exit()

results = []
for querytax in dat:
    for refspecies in ref:
        for i in range(len(ref[refspecies])-1, -1, -1):
            if ref[refspecies][i]["ScientificName"] == querytax:
                results.append((refspecies, ref[refspecies][i]["Rank"], ref[refspecies][i]["ScientificName"]))

if results[0][1] != results[1][1]:
    print("Your best option is: %s" % results[0][0])
    print("Is related at level of %s: %s" % (results[0][1], results[0][2]))
else:
    print("Your bests options are: %s or %s" %(results[0][0], results[1][0]))
    print("Are related at level of %s: %s" % (results[0][1], results[0][2]))
